// SE284 - Winter 2020/2021
// Quiz - JavaScript Drawing
// Name(s):  Ethan Hindes Alex Ruchti

// Tell the code inspection tool that we're writing ES6 compliant code:
// jshint esversion: 6
// Tell the code inspection tool that we're using "developer" classes (console, alert, etc)
// jshint devel:true
// See https://jshint.com/docs/ for more JSHint directives
// jshint unused:false


/**
 * This class represents a cell in our application by using a div
 */
class Cell {

    /**
     * Defines a cell by setting up callbacks for actions when a user interacts with the cell
     * @param drawing Drawing class of which the cell is a member
     */
    constructor(drawing) {
        // Create underlying cell div
        const cell = document.createElement('div');
        cell.classList.add('cell');
        // Set color equal to the background color of the drawing "canvas"
        cell.style.backgroundColor = drawing.getBlankColor();

        /**
         * Color the cell to match the selected color of the drawing
         */
        const doDraw = () => {
            cell.style.backgroundColor = drawing.getColor();
        };

        /**
         * Color setter to allow the cell to be change by operations such as clear
         */
        this.setColor = (color) => {cell.style.backgroundColor = color};

        // Draw when the cell is moused over with a pen if mouse is down
        cell.onmouseenter = () => {
            if(drawing.getIsMouseDown()) doDraw();
        };

        // Draw if the mouse clicks on the cell
        cell.onmousedown = () => {
            doDraw();
        };

        this.cell = cell;

    }

}