// SE284 - Winter 2020/2021
// Quiz - JavaScript Drawing
// Name(s):  Ethan Hindes Alex Ruchti

// Tell the code inspection tool that we're writing ES6 compliant code:
// jshint esversion: 6
// Tell the code inspection tool that we're using "developer" classes (console, alert, etc)
// jshint devel:true
// See https://jshint.com/docs/ for more JSHint directives
// jshint unused:false


const colors = [
    "red",
    "orange",
    "yellow",
    "green",
    "blue",
    "purple",
];

/**
 * This class represents a control panel
 */
class ControlPanel {

    constructor(drawing) {
        /**
         * Used for when we are changing color of the drawing. Reset all button
         * borders to allow for only the selected button to have a unique border
         **/
        const reset_button_borders = () => {
            const buttons = document.getElementsByTagName('button');
            for (let i = 0; i < buttons.length; i++) {
                let current_button = buttons.item(i);
                current_button.style.border = `solid 5px ${current_button.style.backgroundColor}`;
            }
            const input_form = document.getElementById('color_input_form');
            input_form.style.borderColor = input_form.style.backgroundColor;
        }
        /**
         * Populate the color menu with a column of buttons for color select.
         * This method simply adds the default color buttons not the custom select
         * button
         */
        const createColorMenu = () => {
            const color_menu = document.getElementById('color_menu');
            const brush = document.createElement('label');
            brush.innerText = 'Brush Color';
            color_menu.appendChild(brush);
            colors.forEach((color) => {
                const button = document.createElement("button");
                button.style.backgroundColor = color;
                button.innerText = color;
                button.style.border = `solid ${color} 5px`;
                button.className = color;

                button.onclick = () => {
                    drawing.setColor(color);
                    // Set default borders on all other buttons
                    reset_button_borders();
                    // Set black border on selected button
                    button.style.border = 'solid black 5px';
                };
                color_menu.appendChild(button);
            });
            // Set black border for default pen color
            document.getElementsByClassName('red').item(0).style.borderColor = 'black';
        };

        /**
         * Creates the custom color input button and adds it to the color
         * menu
         */
        const createCustomColorInput = () => {
            // Create custom input div
            const color_input_form = document.createElement('div');
            color_input_form.id = 'color_input_form';
            // Text input create
            const color_input = document.createElement('input');
            color_input.type = 'text';
            // Label of the input displaying "custom"
            const label = document.createElement('label');
            label.innerText = 'custom';
            // Show when the input is not a valid color
            const invalid_tag = document.createElement('p');
            invalid_tag.innerText = 'invalid color';
            invalid_tag.id = 'custom_invalid_tag';

            // Add the input to the color input col
            color_input_form.appendChild(label);
            color_input_form.appendChild(color_input);
            const color_menu = document.getElementById('color_menu');
            color_menu.appendChild(color_input_form);

            let valid = false;

            color_input.onkeyup = () => {
                // Default button
                if (color_input.value === '') {
                    reset_button_borders();
                    document.getElementsByClassName('red').item(0).style.borderColor = 'black';
                    drawing.setColor(drawing.getBlankColor());
                    color_input_form.style.backgroundColor = 'grey';
                    color_input_form.style.borderColor = 'grey';
                    valid = true;
                    try {
                        color_input_form.removeChild(invalid_tag);
                    } catch (e) {
                        // do nothing
                    }
                    return;
                }

                const test = document.createElement('button');
                test.style.backgroundColor = '';
                test.style.backgroundColor = color_input.value;

                reset_button_borders();
                color_input_form.style.border = 'solid black 5px';

                if (test.style.backgroundColor === '') {
                    color_input_form.style.backgroundColor = 'grey';
                    color_input_form.appendChild(invalid_tag);
                    valid = false;
                } else {
                    color_input_form.style.backgroundColor = color_input.value;
                    drawing.setColor(color_input.value);
                    color_input_form.removeChild(invalid_tag);
                    valid = true;
                }
            }

            color_input_form.onclick = () => {
                if (valid) {
                    reset_button_borders();
                    color_input_form.style.borderColor = 'black';
                    drawing.setColor(color_input.value);
                }
            }
        };

        /**
         * Adds the custom eraser and clear button to the menu under a tool section
         */
        const createToolsMenu = () => {
            // Create label for tool menu
            const toolMenuLabel = document.createElement('label');
            toolMenuLabel.innerText = 'Tools'
            const toolMenu = document.getElementById('color_menu');
            toolMenu.appendChild(toolMenuLabel);

            // Eraser Button
            const eraserButton = document.createElement("button");
            eraserButton.style.backgroundColor = "grey";
            eraserButton.innerText = "Eraser";
            eraserButton.className = "eraser";

            eraserButton.onclick = () => {
                drawing.setColor(drawing.getBlankColor());
                // Set default borders on all other buttons
                reset_button_borders();
                // Set black border on selected button
            };
            toolMenu.appendChild(eraserButton);

            // Clear Button
            const clearButton = document.createElement("button");
            clearButton.style.backgroundColor = "grey";
            clearButton.innerText = "Clear";
            clearButton.className = "clear";

            clearButton.onclick = () => {
                drawing.resetCanvas();
                // Set default borders on all other buttons
                reset_button_borders();
                // Set black border on selected button
            };
            toolMenu.appendChild(clearButton);

        };

        drawing.setColor(colors[0]);
        createColorMenu();
        createCustomColorInput();
        createToolsMenu();
    }

}