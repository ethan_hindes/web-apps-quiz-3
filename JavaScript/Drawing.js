// SE284 - Winter 2020/2021
// Quiz - JavaScript Drawing
// Name(s):   Ethan Hindes Alex Ruchti

// Tell the code inspection tool that we're writing ES6 compliant code:
// jshint esversion: 6
// Tell the code inspection tool that we're using "developer" classes (console, alert, etc)
// jshint devel:true
// See https://jshint.com/docs/ for more JSHint directives
// jshint unused:false

const blankColor = "#1f3f5f";
class Drawing {
    /**
     * Sets up the drawing with cells of a given width and height
     * and defines required functions for this class to interact with
     * the broke boi paint app
     *
     * @param drawingElement The html element in which to set up the drawing
     * @param width Width of canvas, in cells
     * @param height Height of canvas, in cells
     */
    constructor(drawingElement, width, height) {
        // Set active color
        this.setColor = (color) => {
            drawingElement.selectedColor = color;
        }
        // Getters
        this.getColor = () => drawingElement.selectedColor;
        this.getBlankColor = () => blankColor;
        this.getIsMouseDown = () => drawingElement.ismousedown;
        // 2d array for cells
        const cells = [];

        /**
         * Sets up the cells in the drawing. Uses the given dimensions
         */
        const makeGrid = () => {
            for(let i = 0; i < height; i++) {
                const row = document.createElement("div");
                row.classList.add("row");
                drawingElement.appendChild(row);

                //Cell row
                const cellRow = [];

                // Create the cells and add them to the row
                for(let j = 0; j < width; j++) {
                    //cell object row
                    const cell = new Cell(this);
                    cellRow.push(cell);
                    row.appendChild(cell.cell);
                    cell.cell.id = `cell_${j.toString()}_${i.toString()}`;
                }
                cells.push(cellRow);
            }
        };

        /**
         * Resets all of the cells on the drawing to the blank color
         */
        this.resetCanvas = () => {
            cells.forEach(row => row.forEach(cell => cell.setColor(blankColor)));
        };

        // Initial is mouse down setup
        drawingElement.ismousedown = false;
        drawingElement.onmousedown = () => {
            drawingElement.ismousedown = true;
        };
        drawingElement.onmouseup = () => {
            drawingElement.ismousedown = false;
        };
        makeGrid();
    }
}