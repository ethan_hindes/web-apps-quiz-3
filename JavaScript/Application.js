// SE284 - Winter 2020/2021
// Quiz - JavaScript Drawing
// Name(s):  Ethan Hindes Alex Ruchti

// Tell the code inspection tool that we're writing ES6 compliant code:
// jshint esversion: 6
// Tell the code inspection tool that we're using "developer" classes (console, alert, etc)
// jshint devel:true
// See https://jshint.com/docs/ for more JSHint directives
// jshint unused:false

const rows = 50;
const columns = 90;

window.onload = () => {
    const drawingElements = document.getElementsByClassName("drawing");
    // Loop through all drawings on the page
    Array.from(drawingElements).forEach((drawingElement) => {
        const drawing = new Drawing(drawingElement, columns, rows)
        const controlPanel = new ControlPanel(drawing);
    })
};